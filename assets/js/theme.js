  
  //Menu Toggle Open
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
  //Menu Toggle Closed
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  
  $(document).ready(function(){
    
    //Menu Click Scroll Event
    $( "a.scrollLink" ).click(function( event ) {
        event.preventDefault();
        closeNav();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 100 }, 1500);
    });

    //Sticky Header 
	  $(window).scroll(function(){
      var sticky = $('#home'),
        scroll = $(window).scrollTop();
      
      if (scroll >= 200) sticky.addClass('fixed-sticky');
      else sticky.removeClass('fixed-sticky');
	  });


});
 